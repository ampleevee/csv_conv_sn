package my_search

import (
	"time"
)

func SearchFreshDataForSN(allFilesNames []string) string {

	for _, file := range allFilesNames {

		currentfileDate, _ := time.Parse("02-01-2006_15-04-05", file[:len(file)-4])

		hasThisFileAfter := false

		for _, file1 := range allFilesNames {

			file1Date, _ := time.Parse("02-01-2006_15-04-05", file1[:len(file1)-4])

			if file1Date.After(currentfileDate) {

				hasThisFileAfter = true
				break

			}
		}

		if !hasThisFileAfter {
			return file
		}
	}

	return "error"

}
