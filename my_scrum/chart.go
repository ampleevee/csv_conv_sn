package my_scrum

import (
	"csv_conv_sn/dir_functions"
	"csv_conv_sn/my_conv"
	"csv_conv_sn/my_csv"
	"fmt"
)

func PrintAllSpByStatusesForChart(dir string) {

	// нам нужно сначала получить один статус в хронологическом порядке и сохранить его
	// затем мы получаем следующи статус той же функцией, но результат корректируем с учетом суммы с предыдущим статусом

	// переменная, для хранения хронологии сп по исследуемому статусу
	var status [][]Status

	//status = append(status, GetDynamicsStatusesForChart(Status{"Завершено", 0, 0}, dir))
	//status = append(status, GetDynamicsStatusesForChart(Status{"Тестирование", 0, 0}, dir))
	//status = append(status, GetDynamicsStatusesForChart(Status{"Готово для тестирования", 0, 0}, dir))
	//status = append(status, GetDynamicsStatusesForChart(Status{"В работе", 0, 0}, dir))
	//status = append(status, GetDynamicsStatusesForChart(Status{"Готово для разработки", 0, 0}, dir))
	//status = append(status, GetDynamicsStatusesForChart(Status{"Черновик", 0, 0}, dir))

	status = append(status, GetDynamicsStatusesForChart(Status{"Complete", 0, 0}, dir))
	status = append(status, GetDynamicsStatusesForChart(Status{"Testing", 0, 0}, dir))
	status = append(status, GetDynamicsStatusesForChart(Status{"Ready for testing", 0, 0}, dir))
	status = append(status, GetDynamicsStatusesForChart(Status{"Work in progress", 0, 0}, dir))
	status = append(status, GetDynamicsStatusesForChart(Status{"Ready", 0, 0}, dir))
	status = append(status, GetDynamicsStatusesForChart(Status{"Draft", 0, 0}, dir))

	for i := 1; i < len(status); i++ {
		for j := 0; j < len(status[i]); j++ {
			status[i][j].sp = status[i-1][j].sp + status[i][j].sp
		}
	}
	//for i := 0; i < len(status); i++ {
	//	for j := 0; j < len(status[i]); j++ {
	//		fmt.Println(status[i][j].status, status[i][j].sp)
	//	}
	//}
	for i := 0; i < len(status); i++ {
		fmt.Printf("{\n")
		fmt.Printf("label: '%v',\n", status[i][0].status)

		switch status[i][0].status {
		case "Draft":
			fmt.Printf("backgroundColor: 'rgb(183, 95, 244)',\n")
		case "Ready":
			fmt.Printf("backgroundColor: 'rgb(95, 165, 244)',\n")
		case "Work in progress":
			fmt.Printf("backgroundColor: 'rgb(109, 244, 95)',\n")
		case "Ready for testing":
			fmt.Printf("backgroundColor: 'rgb(244, 207, 95)',\n")
		case "Testing":
			fmt.Printf("backgroundColor: 'rgb(255, 0, 132)',\n")
		case "Complete":
			fmt.Printf("backgroundColor: 'rgb(244, 155, 95)',\n")
		}

		fmt.Printf(" data: [")

		for j := 0; j < len(status[i]); j++ {
			if j < len(status[i])-1 {
				fmt.Printf(" %v,", status[i][j].sp)
				continue
			}
			fmt.Printf(" %v", status[i][j].sp)
		}
		fmt.Printf("]")
		fmt.Printf("\n},")
		fmt.Printf("\n")

	}

}

func GetDynamicsStatusesForChart(status Status, dir string) []Status {

	allCsvFiles := dir_functions.SearchAllCsvFiles(dir)
	var res []Status
	for i := 0; i < len(allCsvFiles); i++ {

		res = append(res, GetValuesInTheFileForChart(status, allCsvFiles[i], dir))

	}

	return res

}

func GetValuesInTheFileForChart(status Status, file string, dir string) Status {

	data := my_csv.CsvParse(file, dir)

	var allSp float64

	allSp = 0

	for i := 1; i < len(data); i++ {

		allSp += my_conv.ThisStrToFloat(data[i][2])

		if data[i][3] == status.status {

			status.sp += my_conv.ThisStrToFloat(data[i][2])

		}

	}

	status.percentage = int((status.sp * 100 / allSp) + 0.5)
	return status

}
