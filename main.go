package main

import (
	"csv_conv_sn/dir_functions"
	"csv_conv_sn/my_scrum"
	"fmt"
)

func main() {

	//dir := "./data/pi_5_2020_s1"
	//dir := "./data/phenix/pi_5_2020_s1"
	dir := "./data/forsazh/pi_5_2020_s1"
	//dir := "./data/for_presentation"

	//sprintStart, sprintFinish := my_time.ConvToTimeSprint("02-09-2019_09-00-00", "13-09-2019_18-00-00")
	//my_scrum.PrintAllBurnDownPoints(allCsvFiles, sprintStart, sprintFinish, dir)

	allCsvFiles := dir_functions.SearchAllCsvFiles(dir)
	team := my_scrum.GetSprintTeam(allCsvFiles, dir)
	// на этом этапе в тим у нас вся команда текущего спринта без посчитанных сп

	theLastFilePath := allCsvFiles[len(allCsvFiles)-1:]
	// здесь мы получили последний актуальный файл

	fmt.Println("---------------------------")
	fmt.Println("--------TEAM RATING--------")
	fmt.Println("---------------------------")
	my_scrum.PrintCurrentTeamRating(team, theLastFilePath[0], dir)
	my_scrum.PrintCurrentTeamRatingForChart(team, theLastFilePath[0], dir)

	fmt.Println("---------------------------")
	fmt.Println("-----SPRINT CUMULATIVE-----")
	fmt.Println("---------------------------")
	my_scrum.PrintAllSpByStatusesForChart(dir)
	fmt.Println("-------------------")
	fmt.Println("-----BURN DOWN-----")
	fmt.Println("-------------------")
	my_scrum.PrintBurnDownChart(dir)
	fmt.Println("-------------------------")
	fmt.Println("-----FULL CUMULATIVE-----")
	fmt.Println("-------------------------")

	my_scrum.PrintFullCumulativeChart("./data/full_data")

	//time.Sleep(300 * time.Second)

}
