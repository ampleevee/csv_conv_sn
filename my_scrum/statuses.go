package my_scrum

import (
	"csv_conv_sn/dir_functions"
	"csv_conv_sn/my_conv"
	"csv_conv_sn/my_csv"
	"fmt"
	"sort"
)

type Status struct {
	status     string
	sp         float64
	percentage int
}

type StatusHistory struct {
	dynamics []Status
}

type AllStatusesDynamics struct {
	dynamics []StatusHistory
}

func PrintAllSpByStatuses(allFiles []string, dir string) {

	theLastFilePath := allFiles[len(allFiles)-1:]
	//fmt.Println(theLastFilePath)
	theFreshestData := my_csv.CsvParse(theLastFilePath[0], dir)

	var currentSprint []Status

	//currentSprint = append(currentSprint, Status{"Черновик", 0, 0})
	//currentSprint = append(currentSprint, Status{"Готово для разработки", 0, 0})
	//currentSprint = append(currentSprint, Status{"В работе", 0, 0})
	//currentSprint = append(currentSprint, Status{"Готово для тестирования", 0, 0})
	//currentSprint = append(currentSprint, Status{"Тестирование", 0, 0})
	//currentSprint = append(currentSprint, Status{"Завершено", 0, 0})

	currentSprint = append(currentSprint, Status{"Draft", 0, 0})
	currentSprint = append(currentSprint, Status{"Ready", 0, 0})
	currentSprint = append(currentSprint, Status{"Work in progress", 0, 0})
	currentSprint = append(currentSprint, Status{"Ready for testing", 0, 0})
	currentSprint = append(currentSprint, Status{"Testing", 0, 0})
	currentSprint = append(currentSprint, Status{"Complete", 0, 0})

	var allSp float64

	allSp = 0

	for i := 0; i < len(theFreshestData); i++ {

		//switch status := theFreshestData[i][3]; status {
		//case "Черновик":
		//	a := my_conv.ThisStrToFloat(theFreshestData[i][2])
		//	currentSprint[0].sp += a
		//	allSp += a
		//case "Готово для разработки":
		//	a := my_conv.ThisStrToFloat(theFreshestData[i][2])
		//	currentSprint[1].sp += a
		//	allSp += a
		//case "В работе":
		//	a := my_conv.ThisStrToFloat(theFreshestData[i][2])
		//	currentSprint[2].sp += a
		//	allSp += a
		//case "Готово для тестирования":
		//	a := my_conv.ThisStrToFloat(theFreshestData[i][2])
		//	currentSprint[3].sp += a
		//	allSp += a
		//case "Тестирование":
		//	a := my_conv.ThisStrToFloat(theFreshestData[i][2])
		//	currentSprint[4].sp += a
		//	allSp += a
		//case "Завершено":
		//	a := my_conv.ThisStrToFloat(theFreshestData[i][2])
		//	currentSprint[5].sp += a
		//	allSp += a
		//
		//}

		switch status := theFreshestData[i][3]; status {
		case "Draft":
			a := my_conv.ThisStrToFloat(theFreshestData[i][2])
			currentSprint[0].sp += a
			allSp += a
		case "Ready":
			a := my_conv.ThisStrToFloat(theFreshestData[i][2])
			currentSprint[1].sp += a
			allSp += a
		case "Work in progress":
			a := my_conv.ThisStrToFloat(theFreshestData[i][2])
			currentSprint[2].sp += a
			allSp += a
		case "Ready for testing":
			a := my_conv.ThisStrToFloat(theFreshestData[i][2])
			currentSprint[3].sp += a
			allSp += a
		case "Testing":
			a := my_conv.ThisStrToFloat(theFreshestData[i][2])
			currentSprint[4].sp += a
			allSp += a
		case "Complete":
			a := my_conv.ThisStrToFloat(theFreshestData[i][2])
			currentSprint[5].sp += a
			allSp += a

		}


	}

	for i := 0; i < len(currentSprint); i++ {
		currentSprint[i].percentage = int((currentSprint[i].sp * 100 / allSp) + 0.5)
	}

	sort.Slice(currentSprint, func(i, j int) bool {

		if currentSprint[i].sp != currentSprint[j].sp {

			return currentSprint[i].sp > currentSprint[j].sp

		} else {

			return currentSprint[i].sp > currentSprint[j].sp
		}
	})

	fmt.Println("Allsp", allSp)

	for i := 0; i < len(currentSprint); i++ {
		fmt.Printf("%v : %v SP (%v%%)", currentSprint[i].status, currentSprint[i].sp, currentSprint[i].percentage)
		draftDynamics := GetDynamicsStatuses(Status{currentSprint[i].status, 0, 0}, dir)

		fmt.Printf("  в динамике:")
		for j := 0; j < len(draftDynamics); j++ {
			if j < len(draftDynamics)-1 {
				fmt.Printf(" %vSP %v%% ->", draftDynamics[j].sp, draftDynamics[j].percentage)
				continue
			}

			fmt.Printf(" %vSP %v%%.", draftDynamics[j].sp, draftDynamics[j].percentage)
		}
		fmt.Printf("\n")

	}

}

func GetDynamicsStatuses(status Status, dir string) []Status {

	allCsvFiles := dir_functions.SearchAllCsvFiles(dir)

	var res []Status

	for i := 0; i < len(allCsvFiles); i++ {

		res = append(res, GetValuesInTheFile(status, allCsvFiles[i], dir))

	}

	return res
}

func GetValuesInTheFile(status Status, file string, dir string) Status {

	data := my_csv.CsvParse(file, dir)

	var allSp float64

	// все стори поинты
	allSp = 0

	for i := 1; i < len(data); i++ {

		allSp += my_conv.ThisStrToFloat(data[i][2])

		if data[i][3] == status.status {

			status.sp += my_conv.ThisStrToFloat(data[i][2])

		}

	}

	status.percentage = int((status.sp * 100 / allSp) + 0.5)
	return status

}
