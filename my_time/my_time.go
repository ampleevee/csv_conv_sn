package my_time

import (
	"strings"
	"time"
)

func ConvToTimeSprint(sprintStart string, sprintFinish string) (time.Time, time.Time) {
	sprintStartRes, _ := time.Parse("02-01-2006_15-04-05", sprintStart)
	sprintFinishRes, _ := time.Parse("02-01-2006_15-04-05", sprintFinish)
	return sprintStartRes, sprintFinishRes
}

func ConvStrToTime(strDate string) time.Time {
	timeDate, _ := time.Parse("02-01-2006_15-04-05", strDate)
	return timeDate
}

func ShortDur(d time.Duration) string {
	s := d.String()
	if strings.HasSuffix(s, "m0s") {
		s = s[:len(s)-2]
	}
	if strings.HasSuffix(s, "h0m") {
		s = s[:len(s)-2]
	}
	return s
}
